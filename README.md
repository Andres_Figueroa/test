# Dockerized Development Container

This is a file containing the installation instructions needed to install the development environment and a proper explanation to use this environment

## Installation 💻

### Docker 🐳

Docker is an app needed to be installed on your computer first, follow [this link](https://docs.docker.com/get-docker/) to get Docker for your system.
To get docker to work virtualization must be enabled on your system:

- [Enable Windows Virtualization](https://support.bluestacks.com/hc/en-us/articles/115003174386-How-can-I-enable-virtualization-VT-on-my-PC-)


### VSCode ⌨

VSCode is an editor needed for this environment to work, you can find it [here](https://code.visualstudio.com/Download).

#### - VSCode Extensions 🛠
VSCode has many extensions that can be installed on it. There are three extensions needed for the development environment:

- Docker
- Remote - Containers
- Remote - WSL

(Note: if you don't know how to get extensions installed on VSCode here is a [link](https://code.visualstudio.com/docs/editor/extension-gallery#:~:text=Browse%20for%20extensions%23,Ctrl%2BShift%2BX) explaining how.)

Any other extension you want to install you are free to do it.

### Create Development Environment 

Once everything is installed and running we proceed to create the Docker Image containing our environment.
To install the development environment is necessary  to obtain the `.devcontainer` folder and the two files inside of it.
- `Dockerfile:` Contains the information needed by docker to install dependencies needed on the system.
- `devcontainer.json:` Contains the information needed by VSCode to install the extensions needed for the development environment to be complete
The folder needs to be inside the project folder we are going to work on or the one we are working on. Once we have it placed the folder must be opened on VSCode and once it is done a window like this:

![](./images/VSCode-BuildingContainer.png)

Click on `Reopen in Container` so VSCode starts building the image. If you want to open another development environment you should open a new window and open the project folder and click reopen in container again.

(Note: When you reopen in a container a folder with a previously created image, the image will be opened and the container started instead of creating a new one)

(Note 2: If any problem regarding WSL is prompted visit this [link](https://docs.microsoft.com/en-us/windows/wsl/) to install it)

## Usage

This development container has the normal functions of any VSCode although it may be different since it is inside the subsystem created by Docker. This Is working with Linux and has everything separated from other projects.

### CodeStream
The environment includes the CodeStream extension which helps to get all the apps used by the team together (Jira, Slack, and GitLab)

![](./images/CodeStream-Interface.png)

This interface divide on the following:
- Feedback Request: This is a place where any developer on the team can ask for any feedback on the code, assign the reviewers and wait for their feedback which will be displayed in this section. (Note: Anyone can se the feedback)
- Codemarks: the place where you can find and make comments about the code in the project. This helps communication inside the team as these comments can be sent via Slack as it will be shown later on this document.
- Issues: The section connected to Jira to be able to keep track, take, create, delete or move any issue.
- My Team: Here you can see your CodeStream's team members, this is a direct way in which CodeStream helps communication within the team. Being able to create different organizations to separate projects and create teams inside of these.

#### - Jira and CodeStream Interface

![](./images/CodeStream-JIRA-Main-Interface.png)
Inside the Jira interface you can:

- Create a filter of the task: applying filters from Jira can be used here to se the status of any task.

![](./images/CodeStream-JIRA-Filter-Interface.png)

- Refresh the tasks list to se the latest
- Open a new issue where you can add the title, assignee, description, a code block (usefull when the issue is link to one), and you can share it to any slack channel in any workspace.

![](./images/CodeStream-JIRA-OpenIssue-Interface.png)

- Manage issues directly from here and even create branches to work on them.

![](./images/CodeStream-JIRA-Issue-Interface.png)


#### - Gitlab: 🦊
Connect to Gitlab with `Integrations`:

<img src="images/git_int.png"  width="260" height="170">

It's possible to create `issues`, see `assigned issues` and request for `merge`:

<img src="images/git_int2.png"  width="250" height="240">

When you `commit` you can write comments, verify edited files and then notify on **Slack**:

<img src="images/git_comm.png"  width="300" height="400">

If `merge` is necessary **CodeStream** lets you:
 - Manage which `branch` to `merge`
 - Add title
 - Add a description
 - Check files with changes

<img src="images/git_mer.png"  width="250" height="400">

### - Slack: 📨
On `commit` is possible to share the code on **Slack**:

<img src="images/git_sla.png"  width="260" height="70">

**Slack** will write:

<img src="images/git_sla2.png"  width="260" height="100">

It's also possible to comment any code and notify on **Slack**. In case it needs revision by the team:

<img src="images/git_sla3.png"  width="350" height="350">

**Slack** will show:

<img src="images/git_sla4.png"  width="260" height="170">
